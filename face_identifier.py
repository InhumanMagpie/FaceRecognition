# import libraries
import cv2
import face_recognition
import numpy as np


# Get a reference to webcam
video_capture = cv2.VideoCapture("/dev/video0")
# Uploading an image of a person as a sample for video recognition
class FaceRecog:
    def __init__(self, path, name):
        image = face_recognition.load_image_file(path)
        self.face_encoding = face_recognition.face_encodings(image, num_jitters=1)[0]
        self.name = name


my_face = FaceRecog("sample_image.jpg", "Me")
known_faces = [
    my_face,
]
# Initialize variables
face_locations = []
face_encodings = []
face_names = []
frame_number = 0
while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_frame = frame[:, :, ::-1]
    # Find all the faces in the current frame of video
    face_locations = face_recognition.face_locations(rgb_frame, model="cnn")
    face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
    face_names = []
    for face_encoding, face_location in zip(face_encodings, face_locations):
        # See if the face is a match for the known face(s)
        match = face_recognition.compare_faces(
            [i.face_encoding for i in known_faces], face_encoding, tolerance=0.50
        )
        for result, face in zip(match, known_faces):
            if result is False:
                continue

            face_names.append([face_location, face.name])
    # Display the results
    for (top, right, bottom, left), name in face_names:
        if not name:
            continue
        # Draw a box around the face
        cv2.rectangle(
            frame, (left, bottom - 25), (right, bottom), (0, 0, 255), cv2.FILLED
        )
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)
        # Display the resulting image
    cv2.imshow("Video", frame)
    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
